﻿using System.Collections.Generic;
using System.Collections.Generic;
using TwoMind.Infrastructure.Model;
using System.Collections;

namespace  MonitorApp
{
    public interface IMoniterService
    {
        
        List<Moniter> GetList(MoniterFilter filter, ResultPaging paging);
        List<Moniter> Moniter(List<Moniter> list,Moniter moniter);
        Moniter GetData(int moniterId);
        Moniter SaveData(Moniter moniter);
        bool DeleteData(int moniter);
    }
}
