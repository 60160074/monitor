﻿using System.Collections.Generic;
using TwoMind.Infrastructure.Model;

namespace  MonitorApp
{
    public interface IDemoRoleService
    {
        List<DemoRole> GetList(DemoRoleFilter filter, ResultPaging paging);
        DemoRole GetData(int roleId);
        DemoRole SaveData(DemoRole role);
        bool DeleteData(int roleId);
    }
}
