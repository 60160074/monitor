﻿using System;
using System.Collections.Generic;

namespace  MonitorApp
{
    public class  WebAppFilter
    {

        public int? WebAppId { get; set; }
        public string WebAppUrl { get; set; }
        public string WebAppName { get; set; }
        public string Port { get; set; }
        public string Status { get; set; }
        public int CreateUserId { get; set; }
        public int ModifyUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }


        public List<WebApp> AreaCollection { get; set; }
    }
}
