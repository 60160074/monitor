﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace  MonitorApp
{
    public class AjaxResultModel
    {
        public AjaxResultModel()
        {
        }

        public string StatusCode { get; set; }
        public object Data { get; set; }
    }
}
