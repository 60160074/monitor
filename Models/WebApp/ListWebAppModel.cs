﻿using System.Collections.Generic;

namespace  MonitorApp
{
    public class ListWebAppModel : BaseListModel
    {
        public ListWebAppModel()
        {
            WebApps = new List<WebAppModel>();
        }

        public string Keyword { get; set; }
        public List<WebAppModel> WebApps { get; protected set; }
    }
}
