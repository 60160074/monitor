﻿using  MonitorApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwoMind.Infrastructure.Model;

namespace  MonitorApp
{
    public class WebAppModel : BaseEditModel
    {
        public int? WebAppId { get; set; }
        public String WebAppUrl { get; set; }
        public string WebAppName { get; set; }
        public string Status { get; set; }
        public string Port { get; set; }
        public  int  CreateUserId { get; set; }
        public int ModifyUserId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        [Obsolete]

        public WebAppModel()
        {
        }

        public WebAppModel(WebApp area)
        {

            ModelHelper.FillDataModel(this, area);
        }
       
        public static List<WebAppModel> CreateModels(List<WebApp> WebApps)
        {

            List<WebAppModel> list = new List<WebAppModel>();
            foreach (WebApp webapp in WebApps)
            {
                list.Add(new WebAppModel(webapp));
            }
            return list;
        }

    }
   

}
