﻿using TwoMind.Infrastructure.Model;

namespace  MonitorApp
{
    public class DemoInternalConfigSection : CentralConfigurationSection
    {
        public static readonly string SectionName = "DemoInternal";

        #region Singleton Implementation
        private static DemoInternalConfigSection _instance = null;

        public static DemoInternalConfigSection GetInstance()
        {
            if (_instance == null)
            {
                _instance = (DemoInternalConfigSection)CentralConfigurationManager.GetSection(SectionName, typeof(DemoInternalConfigSection));
#if DEBUG
                _instance.StorageLocation = @"C:\my\storage\intern_demo1";
                _instance.TempLocation = @"C:\my\storage\intern_demo1";
#endif
            }
            return _instance;
        }
        #endregion

        [CentralConfigurationProperty("StorageLocation", DataType.String)]
        public string StorageLocation
        {
            get { return (string)this["StorageLocation"]; }
            set { this["StorageLocation"] = value; }
        }

        [CentralConfigurationProperty("TempLocation", DataType.String)]
        public string TempLocation
        {
            get { return (string)this["TempLocation"]; }
            set { this["TempLocation"] = value; }
        }
    }
}
