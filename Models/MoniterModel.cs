﻿using  MonitorApp;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TwoMind.Infrastructure.Model;

namespace  MonitorApp
{
    public class MoniterModel : BaseEditModel
    {
        public int? MoniterId { get; set; }
        public int MoniterServer { get; set; }
        public int MoniterApp { get; set; }
        public int MoniterDb { get; set; }
        public int CreateUserId { get; set; }
        public int WebAppId { get; set; }
        public string WebAppName { get; set; }
        public string WebAppUrl { get; set; }
        public string Port { get; set; }
        public DateTime ModifyDate { get; set; }
        public TimeSpan ModifyTime { get; set; }
        [Obsolete]

        public MoniterModel()
        {
        }

        public MoniterModel(Moniter moniter)
        {

            ModelHelper.FillDataModel(this, moniter);
        }
       
        public static List<MoniterModel> CreateModels(List<Moniter> monitors)
        {

            List<MoniterModel> list = new List<MoniterModel>();
            foreach (Moniter monitor in monitors)
            {
                list.Add(new MoniterModel(monitor));
            }
            return list;
        }

    }
   

}
