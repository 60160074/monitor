﻿
using System;
using System.Collections.Generic;

namespace  MonitorApp
{
    public class Moniter
    {

        public int? MoniterId { get; set; }
        public int MoniterServer { get; set; }
        public int MoniterApp { get; set; }
        public int MoniterDb { get; set; }
        public int CreateUserId { get; set; }
        public int WebAppId { get; set; }
        public string Port { get; set; }
        public string WebAppName { get; set; }
        public string WebAppUrl { get; set; }
        public DateTime ModifyDate { get; set; }
        public TimeSpan ModifyTime { get; set; }


     
    }
}
