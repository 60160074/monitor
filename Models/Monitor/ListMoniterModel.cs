﻿using System;
using System.Collections.Generic;

namespace  MonitorApp
{
    public class ListMoniterModel : BaseListModel
    {
        public ListMoniterModel()
        {
            Moniters = new List<MoniterModel>();
        }
        public int? MoniterId { get; set; }
        public int MoniterServer { get; set; }
        public int MoniterApp { get; set; }
        public int MoniterDb { get; set; }
        public int CreateUserId { get; set; }
        public int WebAppId { get; set; }
        public string WebAppName { get; set; }
        public string WebAppUrl { get; set; }
        public string Port { get; set; }

        public int CountError { get; set; }
        public int CountAvailble { get; set; }
        public DateTime ModifyDate { get; set; }
        public TimeSpan ModifyTime { get; set; }

        public string Keyword { get; set; }
        public List<MoniterModel> Moniters { get; protected set; }
    }
}
