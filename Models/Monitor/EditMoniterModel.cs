﻿
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using TwoMind.Infrastructure.Model;

using System.Collections.Generic;
using System;

namespace MonitorApp
{
    public class EditMoniterModel: BaseEditModel
    {

        public int? MoniterId { get; set; }
        public int MoniterServer { get; set; }
        public int MoniterApp { get; set; }
        public int MoniterDb { get; set; }
        public int CreateUserId { get; set; }
        public int WebAppId { get; set; }
        public string WebAppName { get; set; }
        public string WebAppUrl { get; set; }
        public string Port { get; set; }
        public DateTime ModifyDate { get; set; }
        public TimeSpan ModifyTime { get; set; }
        public EditMoniterModel()
        {
             
        }

        public EditMoniterModel(Moniter item)
            : this()
        {
            ModelHelper.FillDataModel(this, item);
        }
        public SelectList Roles { get; protected set; }

        public Moniter ToDataModel(Moniter user = null)
        {
            return ModelHelper.FillDataModel(user ?? new Moniter(), this);
        }

        public SelectList UserList { get; protected set; }
        public void FillLookupList()
        {
            
        }

    }
}
