﻿
using System;
using System.Collections.Generic;

namespace  MonitorApp
{
    public class MoniterFilter
    {

        public int? MonitorId { get; set; }
        public int MoniterServer { get; set; }
        public int MoniterApp { get; set; }
        public int MoniterDb { get; set; }
        public int CreateUserId { get; set; }
        public int WebAppId { get; set; }
        public DateTime ModifyDate { get; set; }
        public TimeSpan ModifyTime { get; set; }
    }
}
