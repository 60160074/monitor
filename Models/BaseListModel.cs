﻿using TwoMind.Infrastructure.WebUI;

namespace  MonitorApp
{
    public class BaseListModel : BaseEditModel, IPagingable
    {
        public BaseListModel()
        {
            Paging = new PagingModel();
        }

        #region IPagingable Implementation
        public virtual PagingModel Paging { get; protected set; }

        public virtual object ToPagingParameter(int pageNo)
        {
            return new { pageNo = pageNo };
        }

        public virtual string ToAdditionalPagingParam()
        {
            return null;
        }
        #endregion
    }
}
