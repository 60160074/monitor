﻿using  MonitorApp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TwoMind.Infrastructure.Entity.Sql;
using TwoMind.Infrastructure.Model;
using System.Transactions;
using System.Net;
using System.Net.NetworkInformation;

namespace MonitorApp
{
    public class MoniterRepository : BaseRepository, IMoniterRepository
    {
        private static readonly Logger _logger = LoggerFactory.GetLogger(typeof(DemoRoleRepository));

        public List<Moniter> GetList(MoniterFilter filter, ResultPaging paging)
        {
            const string func = "GetList";
            try
            {


                string sqlSelect = "Select Moniter.WebAppId,WebAppName, WebAppUrl, Moniter.MoniterServer,Moniter.MoniterApp,Moniter.MoniterDb,Moniter.ModifyTime,Moniter.ModifyDate,WebApp.Port as Port";
                string sqlFromWhere = "from Moniter inner join WebApp on Moniter.WebAppId = Webapp.WebAppId";
                string sqlOrderBy = "ORDER BY MoniterId";
                SqlParameter[] parameters = new SqlParameter[]
                    {
                        


                    };
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet;
                    if (paging == null)
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildNonPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy), parameters);
                    else
                    {
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy, paging), parameters);
                        paging.TotalItem = (int)dataSet.Tables[1].Rows[0][0];
                    }
                    List<Moniter> list = new List<Moniter>();
                    foreach (DataRow row in dataSet.Tables[0].Rows)
                        list.Add(DataHelper.FillDataModel(new Moniter(), row));





                    return list;
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public List<Moniter> Moniter(List<Moniter> list, Moniter moniter)
        {
            const string func = "Monitor";
            try
            {


                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();


                    for (int i = 0; i < list.Count; i++)
                    {
                        int  status = 0;
                        try

                        {

                            
                                Ping myping = new Ping();
                                PingReply rep = myping.Send(list[i].Port, 1000);


                                if (rep.Status != IPStatus.Success)
                                {
                                   list[i].MoniterServer  = 0;
                                 }
                                 else
                                {
                                list[i].MoniterServer = 1;
                                }

                         
                            
                            var url = list[i].WebAppUrl;
                                var req = (HttpWebRequest)WebRequest.Create(url);
                                var res = (HttpWebResponse)req.GetResponse();
                            list[i].MoniterApp = 1;
                        }
                        catch (Exception e)
                        {
                            list[i].MoniterApp = 0;

                        }
                       
                      
                       
                      
                        string sqlText = "UPDATE [Moniter] SET MoniterServer = " +list[i].MoniterServer+","+
                            "MoniterApp = "+list[i].MoniterApp + "," +
                           "MoniterDb = " + list[i].MoniterDb+ "," +
                            " [ModifyDate] =getDate()" +
                            ",[ModiFyTime] = CONVERT(VARCHAR(5), GETDATE(), 114)" +
                            " WHERE WebAppId = " + list[i].WebAppId;
                        DataHelper.ExecuteNonQuery(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, moniter));

                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }
        public Moniter GetData(int IdItem)

        {
            const string func = "GetData";
            try
            {
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet = DataHelper.ExecuteDataSet(connection,
                          "Select Moniter.WebAppId,WebAppName, WebAppUrl, Moniter.MoniterServer,Moniter.MoniterApp,Moniter.MoniterDb,Moniter.ModifyTime,Moniter.ModifyDate,WebApp.Port as Port" +
                       "  from Moniter inner join WebApp on Moniter.WebAppId = Webapp.WebAppId " +
                        " WHERE Moniter.WebAppId ="+IdItem,
                        new SqlParameter("@areaId", IdItem));
                    if (dataSet.Tables[0].Rows.Count != 1)
                        return null;
                    return DataHelper.FillDataModel(new Moniter(), dataSet.Tables[0].Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with user id {1}.", func, IdItem, ex);
                throw ex;
            }
        }



        public virtual Moniter SaveData(Moniter moniter)
        {
            const string func = "SaveData";
            try
            {
                if (moniter == null)
                    throw new ArgumentException("Moniter is null.");
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    if (!moniter.MoniterId.HasValue)
                    {
                        string sqlText = "INSERT INTO [Moniter] (MoniterServer,MoniterApp,MoniterDb,CreateUserId,WebAppId,ModifyDate,ModifyTime) " +
                                     "VALUES (@MoniterServer,@moniterApp,@moniterDb,1,@webAppId,GETDATE(), convert(time(0),getDate())); " +
                                     "SELECT MoniterId = CONVERT(int, SCOPE_IDENTITY());";
                        DataSet dataSet = DataHelper.ExecuteDataSet(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, moniter));
                        moniter.MoniterId = (int)dataSet.Tables[0].Rows[0][0];
                    }
                    else
                    {


                        string sqlText = "UPDATE [TestArea] SET Name = @name, " +
                                   "   ProvinceId = @provinceId, " +
                                   "   ZoneId = @zoneId, " +
                                   "   ModifyDate = GETDATE(), " +

                                   "WHERE AreaId = @areaId";
                        DataHelper.ExecuteNonQuery(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, moniter));
                    }
                }
                return moniter;
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }





        public virtual bool DeleteData(int moniterId)
        {
            const string func = "DeleteData";
            try
            {
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataHelper.ExecuteNonQuery(connection,
                        "DELETE FROM [Moniter] WHERE MoniterId = @moniterId;",


                        new SqlParameter("@moniterId", moniterId));
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with lost id {1}.", func, moniterId, ex);
                throw ex;
            }
        }
    }
}






