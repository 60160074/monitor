﻿using  MonitorApp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TwoMind.Infrastructure.Entity.Sql;
using TwoMind.Infrastructure.Model;

namespace MonitorApp
{
    public class DemoUserRepository : BaseRepository, IDemoUserRepository
    {
        private static readonly Logger _logger = LoggerFactory.GetLogger(typeof(DemoUserRepository));

        public virtual List<DemoUser> GetList(DemoUserFilter filter, ResultPaging paging)
        {
            const string func = "GetList";
            try
            {
                string sqlSelect = "SELECT UserId, Name, UserName, RoleId, IsActive";
                string sqlFromWhere = "FROM [User] " +
                    "WHERE (@name IS NULL OR LOWER(Name) LIKE LOWER(@name)) " +
                    "   AND (@userName IS NULL OR LOWER(UserName) LIKE LOWER(@userName)) " +
                    "   AND (@keyword IS NULL " +
                    "       OR LOWER(Name) LIKE LOWER(@keyword) " +
                    "       OR LOWER(UserName) LIKE LOWER(@keyword)) " +
                    "   AND (@isActive IS NULL OR IsActive = @isActive)";
                string sqlOrderBy = "ORDER BY Name";
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@name", filter == null || filter.Name == null ? (object)DBNull.Value : filter.Name),
                    new SqlParameter("@userName", filter == null || filter.UserName == null ? (object)DBNull.Value : filter.UserName),
                    new SqlParameter("@keyword", filter == null || filter.Keyword == null ? (object)DBNull.Value : filter.Keyword),
                    new SqlParameter("@isActive", filter == null || !filter.IsActive.HasValue ? (object)DBNull.Value : filter.IsActive)
                };
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet;
                    if (paging == null)
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildNonPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy), parameters);
                    else
                    {
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy, paging), parameters);
                        paging.TotalItem = (int)dataSet.Tables[1].Rows[0][0];
                    }
                    List<DemoUser> list = new List<DemoUser>();
                    foreach (DataRow row in dataSet.Tables[0].Rows)
                        list.Add(DataHelper.FillDataModel(new DemoUser(), row));
                    return list;
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public virtual DemoUser GetData(int userId)
        {
            const string func = "GetData";
            try
            {
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet = DataHelper.ExecuteDataSet(connection,
                        "SELECT UserId, Name, UserName, Password, RoleId, IsActive " +
                        "FROM [User] " +
                        "WHERE UserId = @userId",
                        new SqlParameter("@userId", userId));
                    if (dataSet.Tables[0].Rows.Count != 1)
                        return null;
                    return DataHelper.FillDataModel(new DemoUser(), dataSet.Tables[0].Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with user id {1}.", func, userId, ex);
                throw ex;
            }
        }

        public virtual DemoUser SaveData(DemoUser user)
        {
            const string func = "SaveData";
            try
            {
                if (user == null)
                    throw new ArgumentException("User is null.");
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    if (!user.UserId.HasValue)
                    {
                        string sqlText = "INSERT INTO [User] (Name, UserName, Password, RoleId, IsActive, IsSystem) " +
                             "VALUES (@name, @userName, @password, @roleId, @isActive, 0); " +
                             "SELECT Id = CONVERT(int, SCOPE_IDENTITY());";
                        DataSet dataSet = DataHelper.ExecuteDataSet(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, user));
                        user.UserId = (int)dataSet.Tables[0].Rows[0][0];
                    }
                    else
                    {
                        string sqlText = "UPDATE [User] SET Name = @name, " +
                            "   UserName = @userName, " +
                            "   Password = @password, " +
                            "   RoleId = @roleId, " +
                            "   IsActive = @isActive " +
                            "WHERE UserId = @userId";
                        DataHelper.ExecuteNonQuery(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, user));
                    }
                }
                return user;
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public virtual bool DeleteData(int userId)
        {
            const string func = "DeleteData";
            try
            {
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataHelper.ExecuteNonQuery(connection,
                        "DELETE FROM [User] WHERE UserId = @userId",
                        new SqlParameter("@userId", userId));
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with user id {1}.", func, userId, ex);
                throw ex;
            }
        }
    }
}
