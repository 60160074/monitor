﻿using  MonitorApp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TwoMind.Infrastructure.Entity.Sql;
using TwoMind.Infrastructure.Model;
using System.Transactions;
using System.Net;

namespace MonitorApp
{
    public class WebAppRepository : BaseRepository, IWebAppRepository
    {
        private static readonly Logger _logger = LoggerFactory.GetLogger(typeof(DemoRoleRepository));

        public List<WebApp> GetList(WebAppFilter filter, ResultPaging paging)
        {
            const string func = "GetList";
            try
            {
                

                string sqlSelect = "Select Moniter.WebAppId,WebAppName, WebAppUrl, Moniter.MoniterStatus as Status,WebApp.CreateUserId,WebApp.ModifyUserId,WebApp.CreateDate,WebApp.ModifyDate";
                string sqlFromWhere = "from Moniter inner join WebApp on Moniter.WebAppId = Webapp.WebAppId";
                string sqlOrderBy = "ORDER BY MoniterId";
                SqlParameter[] parameters = new SqlParameter[]
                    {
                        new SqlParameter("@Name", filter == null || filter.WebAppName== null ? (object)DBNull.Value : filter.WebAppName),
                      

                    };
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet;
                    if (paging == null)
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildNonPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy), parameters);
                    else
                    {
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy, paging), parameters);
                        paging.TotalItem = (int)dataSet.Tables[1].Rows[0][0];
                    }
                    List<WebApp> list = new List<WebApp>();
                    foreach (DataRow row in dataSet.Tables[0].Rows)
                        list.Add(DataHelper.FillDataModel(new WebApp(), row));

                  

                              

                    return list;
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public List<WebApp> Moniter(List<WebApp>list)
        {
            const string func = "Monitor";
            try
            {


               
              
         
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    

                    for (int i = 0; i < list.Count; i++)
                    {
                        try

                        {

                        if(list[i].Status != "") { 
                            var url = list[i].WebAppUrl;
                            var req = (HttpWebRequest)WebRequest.Create(url);
                            var res = (HttpWebResponse)req.GetResponse();
                            list[i].Status = "Available";
                          

                            }


                        }
                        catch (Exception e)
                        {
                            list[i].Status = e.Message;
                        }

                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }
        public WebApp GetData(int IdItem)

        {
            const string func = "GetData";
            try
            {
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet = DataHelper.ExecuteDataSet(connection,
                      "SELECT *" +
                       "FROM  WebApp " +
                        "WHERE WebAppId = @webAppId",
                        new SqlParameter("@areaId", IdItem));
                    if (dataSet.Tables[0].Rows.Count != 1)
                        return null;
                    return DataHelper.FillDataModel(new WebApp(), dataSet.Tables[0].Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with user id {1}.", func, IdItem, ex);
                throw ex;
            }
        }



        public virtual WebApp SaveData(WebApp area)
        {
            const string func = "SaveData";
            try
            {
                if (area.WebAppId == null)
                    throw new ArgumentException("Area is null.");
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    if (area.WebAppId==0)
                    {
                        string sqlText = "INSERT INTO [Webapp] (WebAppName,WebAppUrl,ModifyUserId,CreateDate,ModifyDate,Port) " +
                                     "VALUES (@webAppName, @webAppUrl,1,GETDATE(),GETDATE(),@port); " +
                                     "SELECT WebAppId = CONVERT(int, SCOPE_IDENTITY());";
                        DataSet dataSet = DataHelper.ExecuteDataSet(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, area));
                        area.WebAppId = (int)dataSet.Tables[0].Rows[0][0];
                    }
                    else
                    {
                     
                       
                        string sqlText = "UPDATE [Webapp] SET WebAppName = @webAppName, " +
                                   "   WebAppUrl = @webAppUrl, " +
                                   "   ModifyDate = GETDATE(), " +
                                   "   Port = @port " +
                                 
                                   "WHERE WebAppId = @webappId";
                        DataHelper.ExecuteNonQuery(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, area));
                    }
                }
                return area;
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }





        public virtual bool DeleteData(int webappId)
        {
            const string func = "DeleteData";
            try
            {
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataHelper.ExecuteNonQuery(connection,
                        "DELETE FROM [WebApp] WHERE WebappId = webappId;",


                        new SqlParameter("@webappId", webappId));
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with lost id {1}.", func, webappId, ex);
                throw ex;
            }
        }
    }
}


 



