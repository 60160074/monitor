﻿using System;
using System.Collections.Generic;
using TwoMind.Infrastructure.Model;

namespace  MonitorApp
{
    public class   MoniterService : BaseService, IMoniterService
    {
        private static readonly Logger _logger = LoggerFactory.GetLogger(typeof(MoniterService));
        private IMoniterRepository _repository;

        public MoniterService(IMoniterRepository repository)
        {
            _repository = repository;
        }

        public virtual List<Moniter> GetList(MoniterFilter filter, ResultPaging paging)
        {
            const string func = "GetList";
            try
            {
                return _repository.GetList(filter, paging);
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }
        public virtual List<Moniter> Moniter(List<Moniter> list,Moniter moniter)
        {
            const string func = "Monitor";
            try
            {
                return _repository.Moniter(list,moniter);
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public virtual Moniter GetData(int moniterId)
        {
            const string func = "GetData";
            try
            {
                return _repository.GetData(moniterId);
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with moniter id {1}.", func, moniterId, ex);
                throw ex;
            }
        }

        public Moniter SaveData(Moniter moniter)
        {
            const string func = "SaveData";
            try
            {
                if (moniter == null)
                    throw new ArgumentException("moniter is null.");
                return _repository.SaveData(moniter);
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public bool DeleteData(int moniterId)
        {
            const string func = "DeleteData";
            try
            {
                return _repository.DeleteData(moniterId);
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with moniter id {1}.", func, moniterId, ex);
                throw ex;
            }
        }
    }
}
