﻿using System;
using System.Collections.Generic;
using TwoMind.Infrastructure.Model;

namespace  MonitorApp
{
    public class   WebAppService : BaseService,IWebAppservice
    {
        private static readonly Logger _logger = LoggerFactory.GetLogger(typeof(WebAppService));
        private IWebAppRepository _repository;

        public WebAppService(IWebAppRepository repository)
        {
            _repository = repository;
        }

        public virtual List<WebApp> GetList(WebAppFilter filter, ResultPaging paging)
        {
            const string func = "GetList";
            try
            {
                return _repository.GetList(filter, paging);
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public virtual WebApp GetData(int areaId)
        {
            const string func = "GetData";
            try
            {
                return _repository.GetData(areaId);
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with area id {1}.", func, areaId, ex);
                throw ex;
            }
        }

        public WebApp SaveData(WebApp area)
        {
            const string func = "SaveData";
            try
            {
                if (area == null)
                    throw new ArgumentException("Area is null.");
                return _repository.SaveData(area);
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public bool DeleteData(int areaId)
        {
            const string func = "DeleteData";
            try
            {
                return _repository.DeleteData(areaId);
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with area id {1}.", func, areaId, ex);
                throw ex;
            }
        }

        public List<WebApp> Moniter(List<WebApp> webapp)
        {
            const string func = "Moniter";
            try
            {
                return _repository.Moniter(webapp);
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with area id {1}.", func, webapp, ex);
                throw ex;
            }
        }
    }
}
