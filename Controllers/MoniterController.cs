﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TwoMind.Infrastructure.Model;
using System.Net.NetworkInformation;
using System.Net;
using TwoMind.Infrastructure.Entity.MySql;
using System.Linq;

namespace MonitorApp
{
    public class MoniterController : BaseController
    {
        private static readonly Logger _logger = LoggerFactory.GetLogger(typeof(MoniterController));

        public static readonly string Name = "Moniter";
        public static readonly string ActionList = "List";
        public static readonly string ActionEdit = "Edit";
        public static readonly string ActionAdd = "Add";
        public static readonly string ActionDelelete = "Delete";
        // /moniterwebapp
        // /moniterwebapp/index
        public IActionResult index()
        {
            return RedirectToAction(MoniterController.ActionList);
        }


        // /moniterwebapp/list


        public IActionResult List(ListMoniterModel model, ListWebAppModel model2)
        {
            const string func = "List";
            try
            {
                IMoniterService moniterservice = new MoniterService(new MoniterRepository());
                List<Moniter> lmoniter = moniterservice.GetList(null, null);
                Moniter moniter = new Moniter();
                List<Moniter> dataMoniters = moniterservice.Moniter(lmoniter, moniter);
                model.Moniters.AddRange(MoniterModel.CreateModels(dataMoniters));

             
                
                Response.Headers.Add("Refresh", "60");

                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }
        public IActionResult Edit(int? id)

        {
            var a = id;

            const string func = "Edit";

            try
            {
                EditMoniterModel model = new EditMoniterModel();
                if (id.HasValue)
                {
                    IMoniterService service = new MoniterService(new MoniterRepository());
                    Moniter moniterwebapp = service.GetData(id.Value);
                    if (moniterwebapp == null)
                        return RedirectToAction(MoniterController.ActionList);
                    model = new EditMoniterModel(moniterwebapp);
                }
                else
                    model = new EditMoniterModel();
                model.FillLookupList();


                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("{0}: Exception caught with id {1}.", func, id, ex);
                throw ex;
            }
        }


        [HttpPost]
        public IActionResult Edit(EditMoniterModel model)//รับค่าโมเดล

        {


            WebApp webapp = new WebApp();

            webapp.WebAppId = model.WebAppId;
            if (webapp.WebAppId == 0)
            {
                ViewBag.status = "add";
            }
            else
            {
                ViewBag.status = "false";
            }
            webapp.WebAppName = model.WebAppName;
            webapp.WebAppUrl = model.WebAppUrl;
            webapp.Port = model.Port;

            IWebAppservice webAppservice = new WebAppService(new WebAppRepository());
            var webappresponse = webAppservice.SaveData(webapp);

            if (ViewBag.status == "add")
            {


                var id = webappresponse.WebAppId;
                Moniter moniter = new Moniter();

           

             
                try

                {
                 

                    Ping myping = new Ping();
                    PingReply rep = myping.Send(webappresponse.Port, 1000);


                    if (rep.Status != IPStatus.Success)
                    {
                        moniter.MoniterServer = 1;
                    }
                    else
                    {
                        moniter.MoniterServer = 0;
                    }

                    
                    
                        var url = webappresponse.WebAppUrl;
                        var req = (HttpWebRequest)WebRequest.Create(url);
                        var res = (HttpWebResponse)req.GetResponse();
                         moniter.MoniterApp = 1;

                }
                catch (Exception e)
                {
                    moniter.MoniterApp = 0;

                }
               

            
                moniter.WebAppId = (int)id;
                IMoniterService moniterservice = new MoniterService(new MoniterRepository());
                var final = moniterservice.SaveData(moniter);

            }

            return RedirectToAction(MoniterController.ActionList);
        }


        public IActionResult Delete(int? editid, string name)

        {

            IWebAppservice service2 = new WebAppService(new WebAppRepository());


            IMoniterService service = new MoniterService(new MoniterRepository());
            Moniter moniterwebapp = service.GetData(editid.Value);

            int delweb = moniterwebapp.WebAppId;
            int delmoniter = (int)moniterwebapp.MoniterId;

            service.DeleteData(delmoniter);
            service2.DeleteData(delweb);     



            return View();
        }



    }
}
