﻿namespace  MonitorApp
{
    public enum BizError
    {
        UserDoesNotExisted,
        UserIsDisabled,
        InvalidPassword,
        DataIsReferenced
    }
}
