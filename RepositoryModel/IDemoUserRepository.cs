﻿using System.Collections.Generic;
using TwoMind.Infrastructure.Model;

namespace MonitorApp
{
    public interface IDemoUserRepository
    {
        List<DemoUser> GetList(DemoUserFilter filter, ResultPaging paging);
        DemoUser GetData(int userId);
        DemoUser SaveData(DemoUser user);
        bool DeleteData(int userId);
    }
}
