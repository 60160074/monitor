﻿using System.Collections.Generic;

using TwoMind.Infrastructure.Model;

namespace MonitorApp
{
    public interface IWebAppRepository
    {
        List<WebApp> GetList(WebAppFilter filter, ResultPaging paging);
        WebApp GetData(int webapp);
        WebApp SaveData(WebApp webapp);
        bool DeleteData(int webappId);
        List<WebApp> Moniter(List<WebApp> list);
    }
}
